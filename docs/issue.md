### Brave rewards issue

- I have been using brave on an Ubuntu 18.04 computer since ~may 2019, earning BAT
- later in 2019 I also started using Brave on my phone, earning BAT
- In Feb/March suddenly no rewards were added, and this lasted for several months
- in June/July 2020 I connected both, the phone and the computer to Uphold, at this point each had approximately 200 BAT in it, that is 400 in total. 
- while the BAT from the Phone wallet were transferred to Uphold, the ones from the computer were not
- in August 2020 I updated the browser and I can see the balance for **both** wallets on the computer:

![screenshot1.png](screenshot1.png)

- however, in my Uphold account, only the funds from the phone are still present


![Screenshot2.png](screenshot2.png)


#### Issues
1. Months in which I received no rewards on computer: February - May 2020
2. Balance on computer wallet is NOT transferred to Uphold NOR have the most recent earings (July 2020) been added to it
3. I have received 4.1667 BAT on Aug 5th which I assume is for the phone wallet, but this DOES NOT correspond to the amount estimated by the app at all (~ the estimate was approx 30 for July 2020)
